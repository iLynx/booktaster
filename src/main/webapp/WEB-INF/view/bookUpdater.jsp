<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Book Taster</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="styles/styles.css" type="text/css"/>
</head>
<body>
<%@include file="header.jsp"%>

<%--results--%>
<table align="center" border=1>
    <thead>
    <tr>
        <th>Index</th>
        <th>Title</th>
        <th>Author</th>
        <th>Genre</th>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${booksList}" var="book" varStatus="bookIndex">
        <tr>
            <td>${bookIndex.index+1}</td>
            <td><c:out value="${book.title}"></c:out></td>
            <td><c:out value="${book.author}"></c:out></td>
            <td><c:out value="${book.genre}"></c:out></td>
        </tr>
    </c:forEach>
    </tbody>

</table>

<%--book updating--%>
<h2 align="center">Please, update chosen book</h2>
<br>
<div align="center">
    <form method="post" action="/BookTaster/update_book">
        Choose a book:
        <select name="currentTitle">
            <option value="---">- none -</option>
            <c:forEach var="book" items="${booksList}" varStatus="bookIndex">
                <option value="<c:out value="${book.title}"/>"><c:out value="${book.title}"/></option>
            </c:forEach>
        </select>
        <br>
        Update title: <input type="text" name="title">
        <br>
        Update Author's First Name: <input type="text" name="firstName">
        <br>
        Update Author's Last Name: <input type="text" name="lastName">
        <br>
        Update Book Genre: <select name="genre">
        <option>science_fiction</option>
        <option>fantasy</option>
        <option>adventure</option>
        <option>magazine</option>
        <option>other</option>
        </select>
        <br>

        <input type="submit" value="Update"/>
    </form>
</div>


<%@include file="footer.jsp"%>
</body>
</html>
