<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Book Taster</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="styles/styles.css" type="text/css"/>
</head>
<body>
  <%@include file="header.jsp"%>

  <table align="center" border=1>
    <thead>
    <tr>
      <th>Index</th>
      <th>Title</th>
      <th>Author</th>
      <th>Genre</th>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${booksList}" var="book" varStatus="myIndex">
      <tr>
        <td>${myIndex.index+1}</td>
        <td><c:out value="${book.title}"></c:out></td>
        <td><c:out value="${book.author}"></c:out></td>
        <td><c:out value="${book.genre}"></c:out></td>
      </tr>
    </c:forEach>
    </tbody>

  </table>

  <%--buttons--%>
  <div align="center">
    <a href="/BookTaster/add_book_redirect?genre=<c:out value="${genre}"/>"><button> Add book </button></a>
    <a href="/BookTaster/update_book_redirect?genre=<c:out value="${genre}"/>"><button> Update book </button></a>
    <a href="/BookTaster/delete_book_redirect?genre=<c:out value="${genre}"/>"><button> Delete book </button></a>
  </div>

  <br>
  <%@include file="footer.jsp"%>



</body>
</html>

