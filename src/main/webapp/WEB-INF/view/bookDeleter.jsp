<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Book Taster</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="styles/styles.css" type="text/css"/>
</head>
<body>
<%@include file="header.jsp"%>

<%--results--%>
<table align="center" border=1>
    <thead>
    <tr>
        <th>Index</th>
        <th>Title</th>
        <th>Author</th>
        <th>Genre</th>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${booksList}" var="book" varStatus="myIndex">
        <tr>
            <td>${myIndex.index+1}</td>
            <td><c:out value="${book.title}"></c:out></td>
            <td><c:out value="${book.author}"></c:out></td>
            <td><c:out value="${book.genre}"></c:out></td>
        </tr>
    </c:forEach>
    </tbody>

</table>

<h2 align="center">Please, select the book which you want to delete</h2>
<br>
<div align="center">
<form method="post" action="/BookTaster/delete_book">
    Choose a book:
    <select name="title">
        <option value="---">- none -</option>
        <c:forEach var="book" items="${allBooksList}" varStatus="myIndex">
            <option value="<c:out value="${book.title}"/>"><c:out value="${book.title}"/></option>
        </c:forEach>
    </select>
    <input type="hidden" name="genre" value="<c:out value="${genre}"/>" />

    <input type="submit" value="Ok"/>
</form>
</div>


<%@include file="footer.jsp"%>
</body>
</html>
