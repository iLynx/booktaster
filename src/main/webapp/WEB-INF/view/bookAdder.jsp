<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Book Taster</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="styles/styles.css" type="text/css"/>
</head>
<body>
  <%@include file="header.jsp"%>

<%--form--%>
  <h2 align="center">Please, fill the gaps</h2>
  <br>
  <div align="center">
      <form action="add_new_book" method="post">
          Book Title: <input type="text" name="title">
          <br>
          Author First Name: <input type="text" name="firstName">
          <br>
          Author Last Name: <input type="text" name="lastName">
          <br>
          Book Genre: <select name="genre">
          <option>science_fiction</option>
          <option>fantasy</option>
          <option>adventure</option>
          <option>magazine</option>
          <option>other</option>
      </select>
          <br>
          <input type="submit" value="Add book">
      </form>
  </div>

  <%@include file="footer.jsp"%>

</body>
</html>
