package com.lynx.bt.datasource;

import com.lynx.bt.util.DBConnector;

import java.io.IOException;
import java.sql.*;

/**
 * @author iLynx
 */
public class JdbcDataSource implements IBookTasterDS {

    private static JdbcDataSource instance;
    private Statement statement;

    public static JdbcDataSource getInstance(){
        if(instance == null){
            return new JdbcDataSource();
        }
        return instance;
    }

    private JdbcDataSource(){} //for inheritance prohibition

    @Override
    public void dsInitialization(){

        try(Connection connection = DBConnector.getConnection()){

            statement = connection.createStatement();
            statement.execute("DROP TABLE IF EXISTS book_list");
            statement.execute("DROP TABLE IF EXISTS book_authors");
            statement.execute("DROP TABLE IF EXISTS book_genres");

            tablesCreation();
            tablesInitialization();


        } catch (SQLException e1){
            e1.printStackTrace();
        } catch (IOException e2){
            e2.printStackTrace();
        }
    }

    public void tablesCreation(){
        try(Connection connection = DBConnector.getConnection()){

            statement = connection.createStatement();

            statement.executeUpdate(
                            "CREATE TABLE book_authors" +
                            "(" +
                            "author_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                            "author_firstName VARCHAR(15)," +
                            "author_lastName VARCHAR(20) NOT NULL" +
                            ");"
            );

            statement.executeUpdate(
                            "CREATE TABLE book_genres" +
                            "(" +
                            "genre_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                            "genre VARCHAR(20) NOT NULL" +
                            ");"
            );

            statement.executeUpdate(
                            "CREATE TABLE book_list" +
                            "(" +
                            "book_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                            "book_title VARCHAR(60) NOT NULL," +
                            "author_id INT NOT NULL," +
                            "genre_id INT NOT NULL," +
                            "CONSTRAINT book_authors_author_id_fk " +
                            "FOREIGN KEY (author_id)" +
                            "REFERENCES book_authors(author_id)," +
                            "CONSTRAINT book_genres_genre_id_fk " +
                            "FOREIGN KEY (genre_id)" +
                            "REFERENCES book_genres(genre_id)" +
                            ");"
            );
            

        } catch (SQLException e1){
            e1.printStackTrace();
        } catch (IOException e2){
            e2.printStackTrace();
        }
    }

    public void tablesInitialization(){
        try(Connection connection = DBConnector.getConnection()){

            statement = connection.createStatement();

            statement.executeUpdate(
                    "INSERT INTO book_genres (genre)" +
                            "VALUES" +
                            "('Science_fiction')," +
                            "('Fantasy')," +
                            "('Adventure')," +
                            "('Magazine')," +
                            "('Other');"
            );

            statement.executeUpdate(
                    "INSERT INTO book_authors (author_firstName, author_lastName)" +
                            "VALUES" +
                            "('George','Lucas')," +
                            "('Suzanne','Collins')," +
                            "('Herman','Melvill')," +
                            "(' ','Marvel Corp')," +
                            "(' ','by Ioann');"
            );

            statement.executeUpdate(
                    "INSERT INTO book_list (book_title, author_id, genre_id)" +
                            "VALUES" +
                            "('Moby-Dick',3,3)," +
                            "('The Hunger Games',2,2)," +
                            "('Catching Fire',2,2)," +
                            "('The Mockingjay',2,2)," +
                            "('Star Wars. Episode I. The Phantom Menace',1,1)," +
                            "('Star Wars. Episode II. Attack of the Clones',1,1)," +
                            "('Star Wars. Episode III. Revenge of the Sith',1,1)," +
                            "('Star Wars. Episode IV. A New Hope',1,1)," +
                            "('Star Wars. Episode V. The Empire Strikes Back',1,1)," +
                            "('Star Wars. Episode VI. Return of the Jedi',1,1)," +
                            "('Star Wars. Episode VII. The Force Awakens',1,1)," +
                            "('Marvel. Wolverine',4,4)," +
                            "('Holy Bible',5,5);"
            );

        } catch (Exception e){
            e.printStackTrace();
        }
    }


    //  <--- DELETE FROM PRODUCTION ALL BELOW THIS LINE --->
    public static void main(String[] args) {

        try(Connection connection = DBConnector.getConnection()){
            JdbcDataSource ds = new JdbcDataSource();
            ds.dsInitialization();

        }catch (Throwable e) {
            e.printStackTrace();
        }
    }



}
