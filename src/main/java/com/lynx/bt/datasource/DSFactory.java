package com.lynx.bt.datasource;

import com.lynx.bt.dataAccessObject.DSType;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @Author iLynx
 */
public class DSFactory {

    public static IBookTasterDS getSource(DSType dsType){
        if(dsType.equals(DSType.COLLECTION)){
            return CollectionDataSource.getInstance();
        } else if(dsType.equals(DSType.JDBC)){
            return JdbcDataSource.getInstance();
        } else return null;
    }

    public static IBookTasterDS getSource(){
        Properties properties;
        DSType dataSourceType = null;

        //read DAO-type from properties file
        try(InputStream inputStream = DSFactory.class.getClassLoader().
        getResourceAsStream("config.properties")){
            properties = new Properties();
            properties.load(inputStream);
        }catch (IOException e1){
            System.err.println(e1.getMessage());
            return null;
        }

        String propValue = properties.getProperty("dao.type");

        if(propValue == null){
            System.err.println("No dao.type property found!");
            return null;
        }

        //set data source
        for(DSType type : DSType.values()) {
            if(propValue.toUpperCase().equals(type.toString())){
                dataSourceType = type;
            }
        }

        return getSource(dataSourceType);
    }
    
}
