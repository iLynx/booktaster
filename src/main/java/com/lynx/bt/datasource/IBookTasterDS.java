package com.lynx.bt.datasource;

import com.lynx.bt.model.Author;
import com.lynx.bt.model.Book;
import com.lynx.bt.model.Genre;

import java.util.List;

/**
 * @Author iLynx
 */
public interface IBookTasterDS {
    void dsInitialization();
}
