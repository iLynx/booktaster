package com.lynx.bt.datasource;

import com.lynx.bt.model.Author;
import com.lynx.bt.model.Book;
import com.lynx.bt.model.Genre;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author iLynx
 */
public class CollectionDataSource implements IBookTasterDS {
    private static List<Book> booksList = new ArrayList<>();
    private static List<Author> authorsList = new ArrayList<>();

    /*private static CollectionDataSource instance;

    public static CollectionDataSource getInstance(){
        if(instance == null){
            return new CollectionDataSource();
        }
        return instance;
    }*/

    //Bill Pugh singleton initialization
    public static class SingletonHolder{
        private final static CollectionDataSource instance = new CollectionDataSource();
    }

    public static CollectionDataSource getInstance(){
        return SingletonHolder.instance;
    }

    private CollectionDataSource(){}  //for inheritance prohibition


    public void add(Object object) throws Exception {
        if(object instanceof Book){
            Book book = (Book) object;
            if(booksList.contains(book)){
                throw new Exception("Such book is already exists!");
            }
            booksList.add(book);
        } else if(object instanceof Author){
            Author author = (Author) object;
            if(authorsList.contains(author)){
                throw new Exception("Such author is already exists!");
            }
            authorsList.add(author);
        } else {
            throw new IllegalArgumentException("Wrong object type");
        }

    }

    public void delete(Object object) throws Exception {
        if(object instanceof Book){
            Book book = (Book) object;
            if(!booksList.contains(book)){
                throw new Exception("Such book doesn't exists!");
            }
            booksList.remove(book);
        } else if(object instanceof Author){
            Author author = (Author) object;
            if(!authorsList.contains(author)){
                throw new Exception("Such author doesn't exists!");
            }
            authorsList.remove(author);
        } else {
            throw new IllegalArgumentException("Wrong object type");
        }
    }

    public void update(int index, Object object){
        if(object instanceof Book){
            Book book = (Book) object;
            booksList.set(index, book);
        } else if(object instanceof Author){
            Author author = (Author) object;
            authorsList.set(index, author);
        } else {
            throw new IllegalArgumentException("Wrong object type");
        }
    }


    public List<Book> getBooksList(){
        return booksList;
    }

    public List<Author> getAuthorsList(){
        return authorsList;
    }

    public List<Book> getBooksByGenre(Genre genre){
        List<Book> list = new ArrayList<>();
        for(Book books : booksList){
            if(books.getGenre() == genre){
                list.add(books);
            }
        }
        return list;
    }

    public Author getAuthorById(int id){
        Author author = null;
        try {
            author = authorsList.get(id);
        }catch(IndexOutOfBoundsException e){
            System.err.println("No such id! " + e.getMessage());
        }
        return author;
    }


    @Override
    public void dsInitialization(){
        //cleaning
        authorsList.clear();
        booksList.clear();

        //authorsList initialization
        authorsList.add(new Author("George", "Lucas")); //0
        authorsList.add(new Author("Suzanne", "Collins")); //1
        authorsList.add(new Author("Herman", "Melvill")); //2
        authorsList.add(new Author("Marvel", "")); //3
        authorsList.add(new Author("by Ioann", "")); //4

        //booksList initialization
        booksList.add(new Book("Mody-Dick", authorsList.get(2), Genre.ADVENTURE));
        booksList.add(new Book("The Hunger Games", authorsList.get(1), Genre.FANTASY));
        booksList.add(new Book("Catching Fire", authorsList.get(1), Genre.FANTASY));
        booksList.add(new Book("The Mockingjay", authorsList.get(1), Genre.FANTASY));
        booksList.add(new Book("Star Wars. Episode I. The Phantom Menace", authorsList.get(0), Genre.SCIENCE_FICTION));
        booksList.add(new Book("Star Wars. Episode II. Attack of the Clones", authorsList.get(0), Genre.SCIENCE_FICTION));
        booksList.add(new Book("Star Wars. Episode III. Revenge of the Sith", authorsList.get(0), Genre.SCIENCE_FICTION));
        booksList.add(new Book("Star Wars. Episode IV. A New Hope", authorsList.get(0), Genre.SCIENCE_FICTION));
        booksList.add(new Book("Star Wars. Episode V. The Empire Strikes Back", authorsList.get(0), Genre.SCIENCE_FICTION));
        booksList.add(new Book("Star Wars. Episode VI. Return of the Jedi", authorsList.get(0), Genre.SCIENCE_FICTION));
        booksList.add(new Book("Star Wars. Episode VII. The Force Awakens", authorsList.get(0), Genre.SCIENCE_FICTION));
        booksList.add(new Book("Marvel. Wolverine", authorsList.get(3), Genre.MAGAZINE));
        booksList.add(new Book("Holy Bible", authorsList.get(4), Genre.OTHER));


    }


    //only for checking
    public static void main(String[] args) throws Exception {
        CollectionDataSource collectionBookTasterDS = CollectionDataSource.getInstance();
        collectionBookTasterDS.dsInitialization();

        for(Author i: collectionBookTasterDS.getAuthorsList()){
            System.out.println(i);
        }
        System.out.println();
        for(Book book: collectionBookTasterDS.getBooksList()){
            System.out.println(book);
        }
    }
}
