package com.lynx.bt.model;

/**
 * @Author iLynx
 */
public class Author {
    private String firstName;
    private String lastName;

    public Author(){
        super();
    }

    public Author (String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    @Override
    public String toString(){
        return firstName + " " + lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (firstName != null ? !firstName.toLowerCase().equals(author.firstName.toLowerCase()) : author.firstName != null) return false;
        if (lastName != null ? !lastName.toLowerCase().equals(author.lastName.toLowerCase()) : author.lastName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.toLowerCase().hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.toLowerCase().hashCode() : 0);
        return result;
    }
}
