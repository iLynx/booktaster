package com.lynx.bt.model;

/**
 * @Author iLynx
 */
public enum Genre {
    SCIENCE_FICTION(1), FANTASY(2), ADVENTURE(3), MAGAZINE(4), OTHER(5);

    private int i;

    Genre(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }

    public static Genre transformGenreFromString(String sGen) {
        for (Genre g : Genre.values()) {
            if (g.toString().equals(sGen.toLowerCase())) {
                return g;
            }
        }

        return Genre.OTHER;
    }
}
