package com.lynx.bt.controller.listener;


import com.lynx.bt.datasource.DSFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionBindingEvent;

/**
 * @Author iLynx
 */

@WebListener()
public class WebAppInitializationContextListener implements ServletContextListener {

    // Public constructor is required by servlet spec
    public WebAppInitializationContextListener() {
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        DSFactory.getSource().dsInitialization();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
      /* This method is invoked when the Servlet Context 
         (the Web application) is undeployed or 
         Application Server shuts down.
      */
    }


}
