package com.lynx.bt.controller.servlet;

import com.lynx.bt.dataAccessObject.BookDAO;
import com.lynx.bt.dataAccessObject.factory.DAOFactory;
import com.lynx.bt.model.Author;
import com.lynx.bt.model.Book;
import com.lynx.bt.model.Genre;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @Author lynx
 */
@WebServlet("/update_book")
public class BookUpdaterServlet extends HttpServlet {

    private BookDAO bookDAO;

    public BookUpdaterServlet(){
        bookDAO = DAOFactory.factoryMethod().getBookDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String genre = request.getParameter("genre");
        String title = request.getParameter("title");
        String authorFirstName = request.getParameter("firstName");
        String authorLastName = request.getParameter("lastName");
        String currentTitle = request.getParameter("currentTitle");

        bookDAO.updateBook(currentTitle, title, authorFirstName, authorLastName, Genre.transformGenreFromString(genre));
        HttpSession session = request.getSession();
        session.setAttribute("booksList", bookDAO.getBooksListByGenre(Genre.transformGenreFromString(genre)));
        session.setAttribute("genre", genre);

        request.getRequestDispatcher("/WEB-INF/view/tableShower.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //empty
    }
}
