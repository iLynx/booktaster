package com.lynx.bt.controller.servlet;

import com.lynx.bt.dataAccessObject.BookDAO;
import com.lynx.bt.dataAccessObject.factory.DAOFactory;
import com.lynx.bt.model.Book;
import com.lynx.bt.model.Genre;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @Author lynx
 */
@WebServlet("/delete_book")
public class BookDeleterServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");

        BookDAO bookDAO = DAOFactory.factoryMethod().getBookDAO();
        bookDAO.deleteBookByTitle(title);

        HttpSession session = request.getSession();

        String genre = request.getParameter("genre");
        List<Book> booksList = bookDAO.getBooksListByGenre(Genre.transformGenreFromString(genre));
        session.setAttribute("booksList", booksList);
        session.setAttribute("genre", genre);

        RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/view/tableShower.jsp");
        rd.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //empty
    }
}
