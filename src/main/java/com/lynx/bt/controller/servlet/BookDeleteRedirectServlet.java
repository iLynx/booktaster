package com.lynx.bt.controller.servlet;

import com.lynx.bt.dataAccessObject.BookDAO;
import com.lynx.bt.dataAccessObject.factory.DAOFactory;
import com.lynx.bt.model.Book;
import com.lynx.bt.model.Genre;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @Author lynx
 */
@WebServlet("/delete_book_redirect")
public class BookDeleteRedirectServlet extends HttpServlet {

    private BookDAO bookDAO;

    public BookDeleteRedirectServlet(){
        bookDAO = DAOFactory.factoryMethod().getBookDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //empty
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String genre = request.getParameter("genre");
        List<Book> booksList = bookDAO.getBooksListByGenre(Genre.transformGenreFromString(genre));
        HttpSession session = request.getSession();
        session.setAttribute("allBooksList", booksList);
        session.setAttribute("genre", genre);

        getServletContext().getRequestDispatcher("/WEB-INF/view/bookDeleter.jsp").forward(request,response);
    }
}
