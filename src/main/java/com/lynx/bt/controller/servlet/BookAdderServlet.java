package com.lynx.bt.controller.servlet;

import com.lynx.bt.dataAccessObject.BookDAO;
import com.lynx.bt.dataAccessObject.factory.DAOFactory;
import com.lynx.bt.model.Author;
import com.lynx.bt.model.Book;
import com.lynx.bt.model.Genre;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @author iLynx
 */
@WebServlet("/add_new_book")
public class BookAdderServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String title = request.getParameter("title");
        String authorFirstName = request.getParameter("firstName");
        String authorLastName = request.getParameter("lastName");
        String genre = request.getParameter("genre");

        //System.err.println(authorFirstName+" "+authorLastName+" "+genre);

        BookDAO bookDAO = DAOFactory.factoryMethod().getBookDAO();

        bookDAO.addBook(new Book(title,new Author(authorFirstName, authorLastName),
                Genre.transformGenreFromString(genre)));

        List<Book> booksList = bookDAO.getBooksListByGenre(Genre.transformGenreFromString(genre));
        HttpSession session = request.getSession();
        session.setAttribute("booksList", booksList);

        RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/view/tableShower.jsp");
        rd.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //empty
    }
}
