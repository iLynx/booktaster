package com.lynx.bt.controller.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author iLynx
 */
@WebServlet("/add_book_redirect")
public class BookAddRedirectServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String genre = request.getParameter("genre"); //not important

        request.getRequestDispatcher("/WEB-INF/view/bookAdder.jsp").forward(request, response);
    }
}
