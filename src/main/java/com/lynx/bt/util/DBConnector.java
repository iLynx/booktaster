package com.lynx.bt.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author iLynx
 */
public class DBConnector {

    public static Connection getConnection() throws SQLException, IOException {

        try (InputStream inputStream = DBConnector.class.getClassLoader().
                getResourceAsStream("config.properties")){

            Properties props = new Properties();
            props.load(inputStream);
            Class.forName(props.getProperty("driver"));

            String driver = props.getProperty("driver");
            if (driver != null) System.setProperty("driver", driver);
            String url = props.getProperty("url");
            String userName = props.getProperty("userName");
            String password = props.getProperty("password");

            return DriverManager.getConnection(url, userName, password);
        } catch (Exception e1) {
            System.err.println(e1.getMessage());
            return null;
        }


        //for testing
        /*String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/bookTaster_db";
        String userName = "root";
        String password = "Bujhm%89";
        try{
            Class.forName(driver);
            System.out.println("driver loaded");
            if (driver != null) System.setProperty("driver", driver);
            return DriverManager.getConnection(url, userName, password);
        } catch (Exception e1) {
            e1.printStackTrace();
            return null;
        }*/

    }
}
