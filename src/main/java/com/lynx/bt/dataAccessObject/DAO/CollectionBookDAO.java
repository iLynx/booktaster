package com.lynx.bt.dataAccessObject.DAO;

import com.lynx.bt.dataAccessObject.BookDAO;
import com.lynx.bt.datasource.CollectionDataSource;
import com.lynx.bt.model.Author;
import com.lynx.bt.model.Book;
import com.lynx.bt.model.Genre;

import java.util.List;

/**
 * @author iLynx
 */
public class CollectionBookDAO implements BookDAO {
    CollectionDataSource dataSource = CollectionDataSource.getInstance();


    @Override
    public void addBook(Book book){
        try{
        dataSource.add(book);
        } catch (Exception e){
            System.err.println(e.getMessage());
        }

    }

    @Override
    public void updateBook(String currentTitle, String updTitle, String updFirstName, String updLastName, Genre updGenre) {
        int index = 0;
        Book currentBook = null;
        Book updatedBook = new Book();
        for (int i=0; i<dataSource.getBooksList().size(); i++){
            if (dataSource.getBooksList().get(i).getTitle().equals(currentTitle)){
                currentBook = dataSource.getBooksList().get(i);
                index = i;
            }
        }
        if(currentBook == null) {return;}
        if(updTitle.equals("")){
            updatedBook.setTitle(currentBook.getTitle());
        }else {
            updatedBook.setTitle(updTitle);
        }
        if(updFirstName.equals("") && updLastName.equals("")){
            updatedBook.setAuthor(currentBook.getAuthor());
        }else {
            updatedBook.setAuthor(new Author(updFirstName,updLastName));
        }
        updatedBook.setGenre(updGenre);
        dataSource.update(index, updatedBook);
    }

    @Override
    public void deleteBookByTitle(String title) {
        try{
            if(title.equals("---")) return;
            Book book = null;
            List<Book> booksList = dataSource.getBooksList();
            for(Book currentBook : booksList){
                if (currentBook.getTitle().equals(title)){
                    book = currentBook;
                }
            }
            dataSource.delete(book);
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    @Override
    public Book getBookById(int id) {
        return dataSource.getBooksList().get(id);
    }

    @Override
    public List<Book> getBooksListByGenre(Genre genre) {
        List<Book> list = dataSource.getBooksByGenre(genre);
        if(list.isEmpty()){
            System.err.println("There are no such books in the catalog");
        }
        return list;
    }

    @Override
    public List<Book> getBooksList() {
        return dataSource.getBooksList();
    }


}
