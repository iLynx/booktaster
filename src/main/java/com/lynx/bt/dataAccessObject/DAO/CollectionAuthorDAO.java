package com.lynx.bt.dataAccessObject.DAO;

import com.lynx.bt.dataAccessObject.AuthorDAO;
import com.lynx.bt.datasource.CollectionDataSource;
import com.lynx.bt.model.Author;

import java.util.List;

/**
 * @author iLynx
 */
public class CollectionAuthorDAO implements AuthorDAO {
    CollectionDataSource dataSource = CollectionDataSource.getInstance();

    @Override
    public void addAuthor(Author author) {
        try{
        dataSource.add(author);
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void updateAuthor(int id, String updatedFirstName, String updatedLastName) {
        Author updatedAuthor = dataSource.getAuthorsList().get(id);
        updatedAuthor.setFirstName(updatedFirstName);
        updatedAuthor.setLastName(updatedLastName);
        dataSource.update(id, updatedAuthor);
    }

    @Override
    public void deleteAuthor(Author author) {
        try{
            dataSource.delete(author);
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    @Override
    public Author getAuthorById(int id) {

        return getAuthorsList().get(id);
    }

    @Override
    public List<Author> getAuthorsList() {
        return dataSource.getAuthorsList();
    }
}
