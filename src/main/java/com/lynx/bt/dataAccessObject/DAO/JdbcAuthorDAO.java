package com.lynx.bt.dataAccessObject.DAO;

import com.lynx.bt.dataAccessObject.AuthorDAO;
import com.lynx.bt.model.Author;
import com.lynx.bt.util.DBConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author iLynx
 */
public class JdbcAuthorDAO implements AuthorDAO {
    @Override
    public void addAuthor(Author author) {
        try(Connection connection = DBConnector.getConnection()){

            PreparedStatement ps0 = connection.prepareStatement(
                    "SELECT * FROM book_authors WHERE author_firstName = ? AND author_lastName = ?"
            );
            ps0.setString(1,author.getFirstName());
            ps0.setString(2,author.getLastName());
            ResultSet rs = ps0.executeQuery();
            if(rs.next()){
                System.out.println("this author already exists!");
                throw new IllegalArgumentException();
            }else {
                PreparedStatement ps = connection.prepareStatement(
                        "INSERT INTO book_authors (author_firstName, author_lastName) VALUES (?,?)"
                );
                ps.setString(1, author.getFirstName());
                ps.setString(2, author.getLastName());
                ps.executeUpdate();
            }

        } catch (IllegalArgumentException e0){
            System.err.println("this author already exists!");
        } catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void updateAuthor(int id, String updatedFirstName, String updatedLastName) {
        try(Connection connection = DBConnector.getConnection()) {
            PreparedStatement ps1 = connection.prepareStatement(
                    "SELECT author_firstName,author_lastName FROM book_authors WHERE author_id = ?"
            );
            ps1.setInt(1, id);

            ResultSet rs = ps1.executeQuery();
            String firstName = null;
            String lastName = null;
            while (rs.next()) {
                firstName = rs.getString(1);
                lastName = rs.getString(2);
            }

            PreparedStatement ps2 = connection.prepareStatement(
                    "UPDATE book_authors SET author_firstName = ?, author_lastName = ?" +
                            "WHERE author_id = ?"
            );

            if (!updatedFirstName.equals("") && updatedLastName.equals("")) {
                ps2.setString(1, updatedFirstName);
                ps2.setString(2, lastName);
            } else if(updatedFirstName.equals("") && !updatedLastName.equals("")){
                ps2.setString(1, firstName);
                ps2.setString(2, updatedLastName);
            } else if(updatedFirstName.equals("") && updatedLastName.equals("")){
                ps2.setString(1, firstName);
                ps2.setString(2, lastName);
            } else {
                ps2.setString(1, updatedFirstName);
                ps2.setString(2, updatedLastName);
            }
            ps2.setInt(3,id);
            ps2.executeUpdate();

        } catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void deleteAuthor(Author author) {
        try(Connection connection = DBConnector.getConnection()){

            PreparedStatement ps = connection.prepareStatement(
                    "DELETE FROM book_authors WHERE author_firstName = ? AND author_lastName = ?"
            );
            ps.setString(1, author.getFirstName());
            ps.setString(2, author.getLastName());
            ps.executeUpdate();

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public Author getAuthorById(int id) {
        Author author = null;

        try(Connection connection = DBConnector.getConnection()){
            PreparedStatement ps = connection.prepareStatement("SELECT author_firstName, author_lastName " +
                    "FROM book_authors WHERE author_id = ?");
            ps.setInt(1,id);

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                String firstName = rs.getString(1);
                String lastName = rs.getString(2);
                author = new Author(firstName, lastName);
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
        return author;
    }

    @Override
    public List<Author> getAuthorsList() {
        List<Author> authorsList = new ArrayList<>();
        try(Connection connection = DBConnector.getConnection()){

            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery("SELECT * FROM book_authors");
            while(rs.next()){
                String firstName = rs.getString(1);
                String lastName = rs.getString(2);
                authorsList.add(new Author(firstName,lastName));
            }

        } catch (Exception e){
            System.err.println(e.getMessage());
        }
        return authorsList;
    }

}
