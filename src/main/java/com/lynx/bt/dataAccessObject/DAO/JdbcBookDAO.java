package com.lynx.bt.dataAccessObject.DAO;

import com.lynx.bt.dataAccessObject.AuthorDAO;
import com.lynx.bt.dataAccessObject.BookDAO;
import com.lynx.bt.dataAccessObject.factory.DAOFactory;
import com.lynx.bt.model.Author;
import com.lynx.bt.model.Book;
import com.lynx.bt.model.Genre;
import com.lynx.bt.util.DBConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author iLynx
 */
public class JdbcBookDAO implements BookDAO {

    private static AuthorDAO authorDAO;

    static{
        authorDAO = DAOFactory.factoryMethod().getAuthorDAO();
    }

    @Override
    public void addBook(Book book) {
        try(Connection connection = DBConnector.getConnection()){

            PreparedStatement ps0 = connection.prepareStatement(
                    "SELECT * FROM book_list WHERE book_title = ? AND author_id = ?"
            );
            ps0.setString(1, book.getTitle());
            ps0.setInt(2, getAuthorIdByName(book.getAuthor().getFirstName(), book.getAuthor().getLastName()));
            ResultSet rs0 = ps0.executeQuery();
            if(rs0.next()){
                System.err.println("Such book already exists!");
            } else {
                PreparedStatement ps = connection.prepareStatement(
                        "INSERT INTO book_list (book_title, author_id, genre_id) VALUES (?,?,?)"
                );
                ps.setString(1, book.getTitle());
                int authorId = getAuthorIdByName(book.getAuthor().getFirstName(), book.getAuthor().getLastName());
                System.err.println("authorId "+authorId);
                ps.setInt(2, authorId);
                int genreId = getGenreIdByName(book.getGenre().toString());
                System.err.println("genreID "+genreId);
                ps.setInt(3, genreId);
                System.err.println();
                ps.executeUpdate();
            }

        } catch (Throwable e){
            e.printStackTrace();
        }
    }

    @Override
    public void updateBook(String currentTitle, String updTitle, String updFirstName, String updLastName, Genre updGenre) {
        try(Connection connection = DBConnector.getConnection()){

            //select current value
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT * FROM book_list WHERE book_title = ?"
            );
            ps.setString(1, currentTitle);
            ResultSet rs = ps.executeQuery();
            String title = null;
            int authorId = 0;
            while(rs.next()){
                title = rs.getString(2);
                authorId = rs.getInt(3);
            }

            //updating
            PreparedStatement ps1 = connection.prepareStatement(
                    "UPDATE book_list SET book_title = ?, author_id = ?, genre_id = ? WHERE book_title = ?"
            );
            ps1.setString(1, updTitle.equals("") ? title : updTitle);
            authorDAO.updateAuthor(authorId, updFirstName, updLastName);
            ps1.setInt(2, authorId);
            ps1.setInt(3, getGenreIdByName(updGenre.toString()));
            ps1.setString(4, currentTitle);
            ps1.executeUpdate();

        } catch (Throwable e){
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBookByTitle(String title) {
        try(Connection connection = DBConnector.getConnection()){
            System.err.println("deleting connected");
            PreparedStatement ps = connection.prepareStatement(
                    "DELETE FROM book_list WHERE book_title = ?"
            );
            ps.setString(1, title);
            ps.executeUpdate();

        } catch (Throwable e){
            e.printStackTrace();
        }
    }

    @Override
    public Book getBookById(int id) {
        Book book = new Book();

        try(Connection connection = DBConnector.getConnection()){

            PreparedStatement ps = connection.prepareStatement(
                    "SELECT * FROM book_list WHERE book_id = ?"
            );
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                book.setTitle(rs.getString(2));
                book.setAuthor(getAuthorById(rs.getInt(3)));
                book.setGenre(getGenreById(rs.getInt(4)));
            }

        } catch (Throwable e){
            e.printStackTrace();
        }

        return book;
    }

    @Override
    public List<Book> getBooksListByGenre(Genre genre) {
        List<Book> booksList = new ArrayList<>();

        try(Connection connection = DBConnector.getConnection()){

            PreparedStatement ps = connection.prepareStatement(
                    "SELECT * FROM book_list WHERE genre_id = ?"
            );

            ps.setInt(1, getGenreIdByName(genre.toString()));

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Book book = new Book();
                book.setTitle(rs.getString(2));
                book.setGenre(genre);
                book.setAuthor(getAuthorById(rs.getInt(3)));
                booksList.add(book);
            }

        } catch (Throwable e){
            e.printStackTrace();
        }

        return booksList;
    }

    @Override
    public List<Book> getBooksList() {
        List<Book> booksList = new ArrayList<>();

        try(Connection connection = DBConnector.getConnection()){

            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery("SELECT * FROM book_list");
            while(rs.next()){
                Book book = new Book();
                book.setTitle(rs.getString(2));
                book.setGenre(getGenreById(rs.getInt(4)));
                book.setAuthor(getAuthorById(rs.getInt(3)));
                booksList.add(book);
            }

        } catch (Throwable e){
            e.printStackTrace();
        }

        return booksList;
    }

    public int getGenreIdByName(String genreName){
        int genreId = 5;
        try(Connection connection = DBConnector.getConnection()){

            PreparedStatement ps = connection.prepareStatement(
                    "SELECT genre_id FROM book_genres WHERE LOWER(genre) = ?"
            );

            ps.setString(1, genreName.toLowerCase());

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                genreId = rs.getInt(1);
            }

        } catch (Throwable e){
            e.printStackTrace();
        }

        return genreId;
    }

    public Author getAuthorById(int authorId){
        Author author = new Author();

        try(Connection connection = DBConnector.getConnection()){

            PreparedStatement ps = connection.prepareStatement(
                    "SELECT author_firstName,author_lastName FROM book_authors WHERE author_id = ?"
            );
            ps.setInt(1, authorId);

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                author.setFirstName(rs.getString(1));
                author.setLastName(rs.getString(2));
            }

        } catch (Throwable e){
            e.printStackTrace();
        }

        return author;
    }

    public Genre getGenreById(int genreId){
        Genre genre = Genre.OTHER;

        try(Connection connection = DBConnector.getConnection()){

            PreparedStatement ps = connection.prepareStatement(
                    "SELECT genre FROM book_genres WHERE genre_id = ?"
            );
            ps.setInt(1, genreId);

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                genre = Genre.transformGenreFromString(rs.getString(1));
            }

        } catch (Throwable e){
            e.printStackTrace();
        }

        return genre;
    }

    public int getAuthorIdByName(String authorFirstName, String authorLastName){
        int authorId = 0;

        try(Connection connection = DBConnector.getConnection()){

            PreparedStatement ps = connection.prepareStatement(
                    "SELECT author_id FROM book_authors WHERE author_firstName = ? AND author_lastName = ?"
            );
            ps.setString(1, authorFirstName);
            ps.setString(2, authorLastName);

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                authorId = rs.getInt(1);
            }
            if(authorId == 0){
                authorDAO.addAuthor(new Author(authorFirstName, authorLastName));
                PreparedStatement ps1 = connection.prepareStatement(
                        "SELECT author_id FROM book_authors WHERE author_firstName = ? AND author_lastName = ?"
                );
                ps1.setString(1, authorFirstName);
                ps1.setString(2, authorLastName);

                ResultSet rs1 = ps1.executeQuery();
                while(rs1.next()){
                    authorId = rs1.getInt(1);
                }
            }

        } catch (Throwable e){
            e.printStackTrace();
        }

        return authorId;
    }

}
