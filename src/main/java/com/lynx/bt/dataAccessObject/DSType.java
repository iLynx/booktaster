package com.lynx.bt.dataAccessObject;

/**
 * @Author iLynx
 */
public enum DSType {
    COLLECTION, JDBC
}
