package com.lynx.bt.dataAccessObject.factory;

import com.lynx.bt.dataAccessObject.AuthorDAO;
import com.lynx.bt.dataAccessObject.BookDAO;
import com.lynx.bt.dataAccessObject.DSType;
import com.lynx.bt.datasource.DSFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author iLynx
 */
public abstract class DAOFactory {

    public static DAOFactory factoryMethod(DSType dsType){
        if(dsType.equals(DSType.COLLECTION)){
            return new CollectionDAOFactory();
        } else if(dsType.equals(DSType.JDBC)){
            return new JdbcDAOFactory();
        } else{
            throw new IllegalArgumentException("wrong data source type!");
        }
    }

    public static DAOFactory factoryMethod(){
        Properties properties;
        DSType dataSourceType = null;

        //read DAO-type from properties file
        try(InputStream inputStream = DSFactory.class.getClassLoader().
                getResourceAsStream("config.properties")){
            properties = new Properties();
            properties.load(inputStream);
        }catch (IOException e1){
            System.err.println(e1.getMessage());
            return null;
        }

        String propValue = properties.getProperty("dao.type");

        if(propValue == null){
            System.err.println("No dao.type property found!");
            return null;
        }

        //set data source
        for(DSType type : DSType.values()) {
            if(propValue.toUpperCase().equals(type.toString())){
                dataSourceType = type;
            }
        }

        return factoryMethod(dataSourceType);
    }

    public abstract BookDAO getBookDAO();

    public abstract AuthorDAO getAuthorDAO();
}
