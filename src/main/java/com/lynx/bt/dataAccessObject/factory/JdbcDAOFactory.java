package com.lynx.bt.dataAccessObject.factory;

import com.lynx.bt.dataAccessObject.AuthorDAO;
import com.lynx.bt.dataAccessObject.BookDAO;
import com.lynx.bt.dataAccessObject.DAO.JdbcAuthorDAO;
import com.lynx.bt.dataAccessObject.DAO.JdbcBookDAO;

/**
 * @Author iLynx
 */
public class JdbcDAOFactory extends DAOFactory {

    @Override
    public BookDAO getBookDAO(){
        return new JdbcBookDAO();
    }

    @Override
    public AuthorDAO getAuthorDAO(){
        return new JdbcAuthorDAO();
    }

}
