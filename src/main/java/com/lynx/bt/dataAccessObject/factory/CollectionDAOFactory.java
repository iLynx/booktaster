package com.lynx.bt.dataAccessObject.factory;

import com.lynx.bt.dataAccessObject.AuthorDAO;
import com.lynx.bt.dataAccessObject.BookDAO;
import com.lynx.bt.dataAccessObject.DAO.CollectionAuthorDAO;
import com.lynx.bt.dataAccessObject.DAO.CollectionBookDAO;

/**
 * @Author iLynx
 */
public class CollectionDAOFactory extends DAOFactory {

    @Override
    public BookDAO getBookDAO(){
        return new CollectionBookDAO();
    }

    @Override
    public AuthorDAO getAuthorDAO(){
        return new CollectionAuthorDAO();
    }

}
