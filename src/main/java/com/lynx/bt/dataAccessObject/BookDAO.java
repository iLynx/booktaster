package com.lynx.bt.dataAccessObject;

import com.lynx.bt.model.Author;
import com.lynx.bt.model.Book;
import com.lynx.bt.model.Genre;

import java.util.List;

/**
 * @Author iLynx
 */
public interface BookDAO {

    void addBook(Book book);
    void updateBook(String currentTitle, String updTitle, String updFirstName, String updLastName, Genre updGenre);
    void deleteBookByTitle(String title);
    Book getBookById(int id); //think about arguments!
    List<Book> getBooksListByGenre(Genre genre);
    List<Book> getBooksList();
}
