package com.lynx.bt.dataAccessObject;

import com.lynx.bt.model.Author;
import com.lynx.bt.model.Genre;

import java.util.List;

/**
 * @Author iLynx
 */
public interface AuthorDAO {

    void addAuthor(Author author);
    void updateAuthor(int id, String updatedFirstName, String updatedLastName);
    void deleteAuthor(Author author);
    Author getAuthorById(int id); //think about arguments!
    List<Author> getAuthorsList();
}
