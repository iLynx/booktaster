package com.lynx.bt.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author iLynx
 */
public class TestAuthorGeneral {

    @Test
    public void testEmptyConstructor(){
        Author author = new Author();

        assertEquals(null, author.getFirstName());
        assertEquals(null, author.getLastName());
    }

    @Test
    public void testConstructor(){
        Author author = new Author("~0303doc//*-- fabDD;", "sdfEW-23)");

        assertEquals("first name","~0303doc//*-- fabDD;",author.getFirstName());
        assertEquals("last name", "sdfEW-23)", author.getLastName());
    }

    @Test
    public void testToStringAuthor(){
        Author author = new Author("dasE#vas-1", "vqwe#cD");

        assertEquals("dasE#vas-1 vqwe#cD", author.toString());
    }


}
