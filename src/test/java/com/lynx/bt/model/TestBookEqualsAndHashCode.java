package com.lynx.bt.model;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * @author iLynx
 */

@RunWith(Parameterized.class)
public class TestBookEqualsAndHashCode {

    private boolean expectedResult;
    private String[] bookTitles;

    private static Book book1;
    private static Book book2;
    private static Author author;

    public TestBookEqualsAndHashCode(boolean expectedResult, String[] bookTitles){
        this.expectedResult = expectedResult;
        this.bookTitles = bookTitles;
    }

    @BeforeClass
    public static void BookLoads(){
        book1 = new Book();
        book2 = new Book();

        author = new Author("Tom", "Hardy");
    }

    @Before
    public void BookInit(){
        book1.setTitle(bookTitles[0]);
        book2.setTitle(bookTitles[1]);
        book1.setAuthor(author); //if authors are different, the tests will fall (so I don't check it here)
        book2.setAuthor(author);
    }

    @Parameterized.Parameters
    public static Collection parameters(){
        return Arrays.asList(new Object[][]{
                {true, new String[]{"title", "TITLE"}},
                {true, new String[]{"title", "TitlE"}},
                {true, new String[]{"asvWEv320/2$#^%", "asvWEv320/2$#^%"}},
                {true, new String[]{"asvWEv320/2$#^%", "ASVweV320/2$#^%"}},
                {true, new String[]{"asvWEv 320/2 $#^%!", "asvWEv 320/2 $#^%!"}},
                {true, new String[]{"", ""}},
                {false, new String[]{"title", "title2"}},
                {false, new String[]{"title", "title "}},
                {false, new String[]{"title title", "title t"}},
                {false, new String[]{"title", "title t"}},
                {false, new String[]{"title", "title t"}},
                {false, new String[]{"baWEb#%2", "baWE b#%2"}},
                {false, new String[]{"baWE b#%2", "baWE b#%2 "}},
                {false, new String[]{"baWE b#%2", " baWE b#%2"}}
        });
    }

    @Test
    public void testBooksEquals(){
        boolean actualResult = book1.equals(book2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testBooksHashCodes(){
        boolean actualResult = book1.hashCode() == book2.hashCode();
        assertEquals(expectedResult, actualResult);
    }
}
