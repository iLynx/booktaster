package com.lynx.bt.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author iLynx
 */
public class TestBookGeneral {

    private static Author author;

    @Before
    public void authorInit(){
        author = new Author("Tom", "Hardy");
    }

    @Test
    public void testEmptyConstructor(){
        Book book = new Book();

        assertEquals("title", null, book.getTitle());
        assertEquals("author", null, book.getAuthor());
        assertEquals("genre", Genre.OTHER, book.getGenre());
    }

    @Test
    public void testNotEmptyConstructor(){
        Book book = new Book("Title", author, Genre.ADVENTURE);

        assertEquals("title", "Title", book.getTitle());
        assertEquals("author", author, book.getAuthor());
        assertEquals("genre", Genre.ADVENTURE, book.getGenre());
    }

    @Test
    public void testToString(){
        Book book = new Book("Title", author, Genre.ADVENTURE);

        String expectedResult = "\"Title\", Author: "+author;
        assertEquals(expectedResult, book.toString());
    }

    @Test
    public void testToStringEmptySet(){
        Book book = new Book();

        String expectedResult = "\"null\", Author: null";
        assertEquals(expectedResult, book.toString());
    }
}
