package com.lynx.bt.datasource;

import com.lynx.bt.model.Author;
import com.lynx.bt.model.Book;
import com.lynx.bt.model.Genre;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * @author Igor Khlaponin
 */

public class TestCollectionDataSource {

    private static CollectionDataSource manager;

    @BeforeClass
    public static void load(){
        manager = CollectionDataSource.getInstance();
    }

    @Test
    public void checkIfClassIsSingleton(){
        assertSame(CollectionDataSource.getInstance(),CollectionDataSource.getInstance());
    }

    @Test
    public void checkDefaultInitiation(){
        manager.dsInitialization();
        List<Book> booksList = manager.getBooksList();
        List<Author> authorsList = manager.getAuthorsList();

        manager.dsInitialization();
        List<Book> booksList2 = manager.getBooksList();
        List<Author> authorsList2 = manager.getAuthorsList();

        assertNotEquals(0, booksList.size());
        assertNotEquals(0, authorsList.size());

        assertEquals(booksList.size(), booksList2.size());
        assertEquals(authorsList.size(), authorsList2.size());
    }

    @Test
    public void checkDefaultInitiationValues() {
        manager.dsInitialization();
        List<Book> booksList = manager.getBooksList();
        List<Author> authorsList = manager.getAuthorsList();

        assertNotEquals(booksList, authorsList);
        assertEquals(5, authorsList.size());
        assertEquals(13, booksList.size());

        assertThat(booksList, hasItems(new Book("Mody-Dick", authorsList.get(2), Genre.ADVENTURE),
                new Book("Catching Fire", authorsList.get(1), Genre.FANTASY)));
        assertThat(booksList, not(hasItems(new Book("Mody - Dick", authorsList.get(2), Genre.ADVENTURE),
                new Book("Catching Fire", authorsList.get(1), Genre.FANTASY))));
        assertThat(booksList, not(hasItem(new Book("Mody-Dick", authorsList.get(3), Genre.ADVENTURE))));

        assertThat(authorsList, hasItems(new Author("George", "Lucas"),new Author("Herman", "Melvill")));
        assertThat(authorsList, not(hasItems(new Author("George", "Melvill"), new Author("Herman", "Lucas"))));
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAddInstanceOf() throws Exception{
        manager.dsInitialization();
        manager.add(new Object());
    }

    @Test(expected = Exception.class)
    public void checkAddOfExistingBook() throws Exception{
        manager.dsInitialization();
        manager.add(new Book("Mody-Dick", manager.getAuthorsList().get(2), Genre.ADVENTURE));
    }

    @Test
    public void checkAddOfNewBook() throws Exception{
        manager.dsInitialization();
        manager.add(new Book("New Title", manager.getAuthorsList().get(0),Genre.OTHER));

        assertThat(manager.getBooksList(), hasItem(new Book("New Title", manager.getAuthorsList().get(0),Genre.OTHER)));
    }

    @Test(expected = Exception.class)
    public void checkAddOfExistingAuthor() throws Exception{
        manager.dsInitialization();
        manager.add(new Author("George", "Lucas"));
    }

    @Test
    public void checkAddOfNewAuthor() throws Exception{
        manager.dsInitialization();
        manager.add(new Author("John", "Snow"));

        assertThat(manager.getAuthorsList(), hasItem(new Author("John", "Snow")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkDeleteOfWrongObject() throws Exception {
        manager.dsInitialization();
        manager.delete(new Object());
    }

    @Test(expected = Exception.class)
    public void checkDeleteOfNotExistingBook() throws Exception{
        manager.dsInitialization();
        manager.delete(new Book());
    }

    @Test
    public void checkDeleteOfExistingBook() throws Exception {
        manager.dsInitialization();
        manager.delete(new Book("Mody-Dick", manager.getAuthorsList().get(2), Genre.ADVENTURE));

        assertThat(manager.getBooksList(), not(hasItem(new Book("Mody-Dick", manager.getAuthorsList().get(2), Genre.ADVENTURE))));
    }

    @Test(expected = Exception.class)
    public void checkDeleteOfNotExistingAuthor() throws Exception{
        manager.dsInitialization();
        manager.delete(new Author());
    }

    @Test
    public void checkDeleteOfExistingAuthor() throws Exception {
        manager.dsInitialization();
        manager.delete(new Author("George", "Lucas"));

        assertThat(manager.getAuthorsList(), not(hasItem(new Author("George", "Lucas"))));
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkUpdateOfWrongObject(){
        manager.update(0, new Object());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void checkUpdateOfBookWithWrongId(){ //with wrong id
        manager.dsInitialization();
        manager.update(manager.getBooksList().size()+1,new Book());
    }

    @Test
    public void checkUpdateOfBook(){
        manager.dsInitialization();
        int id = 0;
        Book updatedBook = manager.getBooksList().get(id); //the book that will be updated
        //new book that will replace old book
        Book newBook = new Book("ModyDick", manager.getAuthorsList().get(2), Genre.ADVENTURE);
        manager.update(id, newBook); //updating

        assertThat(manager.getBooksList(), hasItem(newBook));
        assertThat(manager.getBooksList(), not(hasItem(updatedBook)));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void checkUpdateOfAuthorWithWrongId(){
        manager.dsInitialization();
        manager.update(manager.getAuthorsList().size()+1, new Author());
    }

    @Test
    public void checkUpdateOfAuthor(){
        manager.dsInitialization();
        int id = 0;
        Author updatedAuthor = manager.getAuthorsList().get(id);
        Author newAuthor = new Author("Gregory", "JackShepard");
        manager.update(id, newAuthor);

        assertThat(manager.getAuthorsList(), hasItem(newAuthor));
        assertThat(manager.getAuthorsList(), not(hasItem(updatedAuthor)));
    }

    @Test
    public void checkListByGenre(){
        manager.dsInitialization();
        List<Book> booksList = manager.getBooksByGenre(Genre.SCIENCE_FICTION);

        for(Book book :booksList){
            assertEquals(Genre.SCIENCE_FICTION, book.getGenre());
        }

    }
}
