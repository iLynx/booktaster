package com.lynx.bt.dao;

import com.lynx.bt.dataAccessObject.DAO.CollectionAuthorDAO;
import com.lynx.bt.dataAccessObject.DAO.CollectionBookDAO;
import com.lynx.bt.dataAccessObject.DSType;
import com.lynx.bt.dataAccessObject.factory.DAOFactory;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Igor Khlaponin
 */


public class TestCollectionDaoFactory {

    private static DAOFactory dao;

    @BeforeClass
    public static void init(){
        dao = DAOFactory.factoryMethod(DSType.COLLECTION);
    }

    @Test
    public void checkGetMethods(){
        assertEquals(new CollectionBookDAO().getClass(),dao.getBookDAO().getClass());
        assertEquals(new CollectionAuthorDAO().getClass(), dao.getAuthorDAO().getClass());
    }

}

