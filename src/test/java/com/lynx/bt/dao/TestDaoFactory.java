package com.lynx.bt.dao;

import com.lynx.bt.dataAccessObject.DSType;
import com.lynx.bt.dataAccessObject.factory.CollectionDAOFactory;
import com.lynx.bt.dataAccessObject.factory.DAOFactory;
import com.lynx.bt.dataAccessObject.factory.JdbcDAOFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author Igor Khlaponin
 */

@RunWith(Parameterized.class)
public class TestDaoFactory {

    private Class<? extends DAOFactory> expectedDataSource;
    private DSType actualDataSourceType;

    public TestDaoFactory(Class<? extends DAOFactory> expectedDataSource, DSType actualDataSourceType){
        this.expectedDataSource = expectedDataSource;
        this.actualDataSourceType = actualDataSourceType;
    }

    @Parameterized.Parameters
    public static Collection parameters(){
        return Arrays.asList(new Object[][]{
                {CollectionDAOFactory.class, DSType.COLLECTION},
                {JdbcDAOFactory.class, DSType.JDBC}
        });
    }

    @Test
    public void checkOfDaoFactoryGetting(){
        assertEquals(expectedDataSource, DAOFactory.factoryMethod(actualDataSourceType).getClass());
    }

}
