package com.lynx.bt.dao;

import com.lynx.bt.dataAccessObject.DAO.CollectionAuthorDAO;
import com.lynx.bt.dataAccessObject.DSType;
import com.lynx.bt.dataAccessObject.factory.DAOFactory;
import com.lynx.bt.datasource.CollectionDataSource;
import com.lynx.bt.model.Author;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * @author Igor Khlaponin
 */


public class TestCollectionAuthorDao {

    private static CollectionDataSource dataSource;
    private static CollectionAuthorDAO dao; //?

    @BeforeClass
    public static void init(){
        dataSource = CollectionDataSource.getInstance();
        dao = (CollectionAuthorDAO) DAOFactory.factoryMethod(DSType.COLLECTION).getAuthorDAO();//?
    }

    @Test
    public void checkAddNewObject(){
        dataSource.dsInitialization();
        dao.addAuthor(new Author("Black", "SilverUfa"));

        assertThat(dataSource.getAuthorsList(), hasItem(new Author("Black", "SilverUfa")));
    }

    @Test
    public void checkAuthorUpdate(){
        dataSource.dsInitialization();
        int id = 0;
//        Author updatedAuthor = dataSource.getAuthorsList().get(id);
        Author updatedAuthor = new Author("George", "Lucas");
        Author newAuthor = new Author("Gregory", "JackShepard");
        dao.updateAuthor(id, newAuthor.getFirstName(), newAuthor.getLastName());

        assertThat(dataSource.getAuthorsList(), not(hasItem(updatedAuthor)));
        assertThat(dataSource.getAuthorsList(), hasItem(newAuthor));
    }

    @Test
    public void checkAuthorDelete(){
        dataSource.dsInitialization();
        Author deletedAuthor = new Author("George", "Lucas");
        dao.deleteAuthor(deletedAuthor);

        assertThat(dataSource.getAuthorsList(), not(hasItem(deletedAuthor)));
    }

    @Test
    public void checkAuthorsList(){
        assertEquals(dao.getAuthorsList(), dataSource.getAuthorsList());
    }

    @Test
    public void checkGettingTheAuthorById(){
        dataSource.dsInitialization();

        Author checkedAuthor1 = new Author("George", "Lucas");
        Author checkedAuthor2 = new Author("Suzanne", "Collins");
        Author checkedAuthor3 = new Author("Herman", "Melvill");
        Author checkedAuthor4 = new Author("Marvel", "");
        Author checkedAuthor5 = new Author("by Ioann", "");

        assertEquals(dao.getAuthorById(0), checkedAuthor1);
        assertEquals(dao.getAuthorById(1), checkedAuthor2);
        assertEquals(dao.getAuthorById(2), checkedAuthor3);
        assertEquals(dao.getAuthorById(3), checkedAuthor4);
        assertEquals(dao.getAuthorById(4), checkedAuthor5);
    }
}
