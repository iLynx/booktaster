package com.lynx.bt.dao;

import com.lynx.bt.dataAccessObject.BookDAO;
import com.lynx.bt.dataAccessObject.DSType;
import com.lynx.bt.dataAccessObject.factory.DAOFactory;
import com.lynx.bt.datasource.CollectionDataSource;
import com.lynx.bt.model.Author;
import com.lynx.bt.model.Book;
import com.lynx.bt.model.Genre;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * @author Igor Khlaponin
 */
public class TestCollectionBookDao {

    private static CollectionDataSource dataSource;
    private static BookDAO dao;

    @BeforeClass
    public static void prepareTest(){
        dataSource = CollectionDataSource.getInstance();
        dao = DAOFactory.factoryMethod(DSType.COLLECTION).getBookDAO();
    }

    @Test
    public void checkAddNewBook(){ //think about same objects!!
        dataSource.dsInitialization();
        Book book = new Book("The Hunger Games", dataSource.getAuthorsList().get(1), Genre.FANTASY);
        dao.addBook(book); //add book to the list
        assertThat(dataSource.getBooksList(), hasItem(book));
    }

    @Test
    public void checkUpdateBookWithNoChanges(){
        dataSource.dsInitialization();
        Book book = new Book("The Hunger Games", dataSource.getAuthorsList().get(1), Genre.FANTASY);
        dao.updateBook("The Hunger Games", "", "", "", Genre.FANTASY);
        assertEquals("The Hunger Games", dataSource.getBooksList().get(1).getTitle());
        assertEquals(new Author("Suzanne", "Collins"), dataSource.getBooksList().get(1).getAuthor());
    }

    @Test
    public void checkUpdateBookTitle(){
        dataSource.dsInitialization();
        Book book = new Book("The Hunger Games", dataSource.getAuthorsList().get(1), Genre.FANTASY);
        dao.updateBook("The Hunger Games", "Hunger Games", book.getAuthor().getFirstName(),
                book.getAuthor().getLastName(), Genre.FANTASY);
        assertEquals("Hunger Games", dataSource.getBooksList().get(1).getTitle());
        assertEquals(book.getAuthor(), dataSource.getBooksList().get(1).getAuthor());
    }

    @Test
    public void checkUpdateBookAuthorFirstName(){
        dataSource.dsInitialization();
        Book book = new Book("The Hunger Games", dataSource.getAuthorsList().get(1), Genre.FANTASY);
        dao.updateBook("The Hunger Games", "", "Jack", book.getAuthor().getLastName(), Genre.FANTASY);

        assertEquals("Jack", dataSource.getBooksList().get(1).getAuthor().getFirstName());
        assertEquals(book.getAuthor().getLastName(), dataSource.getBooksList().get(1).getAuthor().getLastName());

        assertThat(dataSource.getBooksList() ,
                hasItem(new Book("The Hunger Games", new Author("Jack", book.getAuthor().getLastName()), Genre.FANTASY)));

    }

    @Test
    public void checkUpdateBookAuthorLastName(){
        dataSource.dsInitialization();
        Book book = new Book("The Hunger Games", dataSource.getAuthorsList().get(1), Genre.FANTASY);
        dao.updateBook("The Hunger Games", "", book.getAuthor().getFirstName(), "Shepard", Genre.FANTASY);

        assertEquals(book.getAuthor().getFirstName(), dataSource.getBooksList().get(1).getAuthor().getFirstName());
        assertEquals("Shepard", dataSource.getBooksList().get(1).getAuthor().getLastName());

        assertThat(dataSource.getBooksList() ,
                hasItem(new Book("The Hunger Games", new Author(book.getAuthor().getFirstName(), "Shepard"), Genre.FANTASY)));

    }

    @Test
    public void checkDeleteBookByEmptyTitle(){
        dataSource.dsInitialization();
        String title = "---";
        int bookListSizeBeforeDeleting = dataSource.getBooksList().size();
        dao.deleteBookByTitle(title);
        int bookListSizeAfterDeleting = dataSource.getBooksList().size();
        assertEquals(bookListSizeBeforeDeleting, bookListSizeAfterDeleting);
    }

    @Test
    public void checkDeleteBookByTitle(){
        dataSource.dsInitialization();
        Book deletedBook = new Book("Mody-Dick", dataSource.getAuthorsList().get(2), Genre.ADVENTURE);
        String title = deletedBook.getTitle();
        dao.deleteBookByTitle(title);
        assertThat(dataSource.getBooksList(), not(hasItem(deletedBook)));
    }

    @Test
    public void checkGetBookById(){
        dataSource.dsInitialization();
        int id = 1;
        assertEquals(dao.getBookById(id), dataSource.getBooksList().get(id));
    }

    @Test
    public void checkGetBooksListByGenre(){
        dataSource.dsInitialization();
        List<Book> booksList = dao.getBooksListByGenre(Genre.SCIENCE_FICTION);

        for(int i=0; i<booksList.size(); i++){
            assertEquals(Genre.SCIENCE_FICTION, booksList.get(i).getGenre());
        }
    }
}
